package com.lagou.edu.factory;

import com.lagou.edu.annotation.Autowired;
import com.lagou.edu.annotation.Component;
import com.lagou.edu.annotation.Repository;
import com.lagou.edu.annotation.Service;
import com.lagou.edu.utils.PackageUtil;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 应癫
 *
 * 工厂类，生产对象（使用反射技术）
 */
public class BeanFactory {

    /**
     * 任务一：读取解析xml，通过反射技术实例化对象并且存储待用（map集合）
     * 任务二：对外提供获取实例对象的接口（根据id获取）
     */

    private static Map<String,Object> map = new HashMap<>();  // 存储对象单例池

    static {
        // 任务一：读取解析xml，通过反射技术实例化对象并且存储待用（map集合）
        // 加载xml
        InputStream resourceAsStream = BeanFactory.class.getClassLoader().getResourceAsStream("beans.xml");
        // 解析xml
        SAXReader saxReader = new SAXReader();
        try {
            Document document = saxReader.read(resourceAsStream);
            Element rootElement = document.getRootElement();
            //获取xml中scan标签
            Element scan = rootElement.element("scan");
            //取得xml中scan标签package的值
            String packageName = scan.attributeValue("package");
            //通过工具类获得包下所有类的全限定类名
            List<String> classNameList = PackageUtil.getClassName(packageName);
            for (int i = 0; i < classNameList.size(); i++) {
                Class<?> clazz = Class.forName(classNameList.get(i));
                String splitClassName [] = classNameList.get(i).split("\\.");
                String className;
                Object o;
                //判断使用了@Component注解的类
                if(clazz.isAnnotationPresent(Component.class)){
                    // 实例化之后的对象
                    o = clazz.getDeclaredConstructor().newInstance();
                    // 获取Component对象
                    Component component = o.getClass().getAnnotation(Component.class);
                    if("".equals(component.value())){
                        className = splitClassName[splitClassName.length-1];
                        className = (new StringBuilder()).append(Character.toLowerCase(className.charAt(0))).append(className.substring(1)).toString();
                    }else{
                        className = component.value();
                    }
                    // 存储到map中待用
                    map.put(className,o);
                }
                //判断使用了@Service注解的类
                if(clazz.isAnnotationPresent(Service.class)){
                    // 实例化之后的对象
                    o = clazz.getDeclaredConstructor().newInstance();
                    // 获取Component对象
                    Service service = o.getClass().getAnnotation(Service.class);
                    if("".equals(service.value())){
                        className = splitClassName[splitClassName.length-1];
                        className = (new StringBuilder()).append(Character.toLowerCase(className.charAt(0))).append(className.substring(1)).toString();
                    }else{
                        className = service.value();
                    }
                    // 存储到map中待用
                    map.put(className,o);
                }
                //判断使用了@Repository注解的类
                if(clazz.isAnnotationPresent(Repository.class)){
                    // 实例化之后的对象
                    o = clazz.getDeclaredConstructor().newInstance();
                    // 获取Component对象
                    Repository repository = o.getClass().getAnnotation(Repository.class);
                    if("".equals(repository.value())){
                        className = splitClassName[splitClassName.length-1];
                        className = (new StringBuilder()).append(Character.toLowerCase(className.charAt(0))).append(className.substring(1)).toString();
                    }else{
                        className = repository.value();
                    }
                    // 存储到map中待用
                    map.put(className,o);
                }
            }
            // 依赖赋值
            for(Map.Entry<String, Object> kv : map.entrySet()){
                String id = kv.getKey();
                Object object = kv.getValue();
                Field[] declaredFields = object.getClass().getDeclaredFields();
                for (int i = 0; i < declaredFields.length; i++) {
                    if(declaredFields[i].isAnnotationPresent(Autowired.class)){
                        if("".equals(declaredFields[i].getAnnotation(Autowired.class).value())){
                            declaredFields[i].setAccessible(true);
                            declaredFields[i].set(object,map.get(declaredFields[i].getName()));
                        }else{
                            declaredFields[i].setAccessible(true);
                            declaredFields[i].set(object,map.get(declaredFields[i].getAnnotation(Autowired.class).value()));
                        }
                    }
                }
            }
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }finally {
            System.out.println("IOC容器配置完成");
        }
    }


    // 任务二：对外提供获取实例对象的接口（根据id获取）
    public static  Object getBean(String id) {
        return map.get(id);
    }

}
